public with sharing class BuildingGeolocation {

    private static final String API_KEY  = 'AIzaSyAsXM6czYLqwQSlzt0d7CM2oT1NtuRlMKM';
    private static final String GEOCODE_URI = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';

    @future(callout=true) 
    public static void getLocation(Id buildingId) {
        Building__c b = [SELECT id, address_City__c, address_State__c, address_Street__c, address_Postal_Code__c 
                        FROM building__c WHERE id =: buildingId];
        String address = '';
        List<String> addressParts = new List<String> {b.address_City__c, b.address_State__c, 
                                                    b.address_Street__c, b.address_Postal_Code__c};
        for (String p : addressParts) {
            if (!String.isBlank(p)) {
                address += p + ',';
            }
        }

        if (String.isBlank(address)) {
            System.debug(LoggingLevel.ERROR, 'No address provided');
            return;
        }
        System.debug(LoggingLevel.FINEST, address);

        address = EncodingUtil.urlEncode(address, 'UTF-8');
        String uri = GEOCODE_URI + address +  '&key=' + API_KEY;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(uri);
        request.setMethod('GET');
        request.setTimeout(10000);
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
        //    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            System.debug(response.getBody());
            System.debug(LoggingLevel.FINEST, 'RESPONSE OK!!');

            Map<String, Double> loc = parseLocation(response.getBody());
            if (loc.get('lng') != null && loc.get('lat') != null) {
                b.location__latitude__s = loc.get('lat');
                b.location__longitude__s = loc.get('lng');
                update b;
            }
        }
    }

    private static Map<String, Decimal> parseLocation (String respBody) {
        Map<String, Decimal> output = new Map<String, Decimal>();
        JSONParser parser = JSON.createParser(respBody);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')) {
                    parser.nextToken();
                    while (parser.nextToken() != JSONToken.END_OBJECT){
                        String txt = parser.getText();
                        parser.nextToken();
                        if (txt == 'lat') {
                            output.put('lat', parser.getDoubleValue());
                        } else if (txt == 'lng') {
                            output.put('lng', parser.getDoubleValue());
                        }
                    }
            }
        }
        return output;
    }   
}