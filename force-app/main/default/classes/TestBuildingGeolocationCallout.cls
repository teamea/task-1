@isTest
public with sharing class TestBuildingGeolocationCallout {

    @isTest static void testInsertBuilding(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetBuildingGeolocationResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);        
        
        Building__c building = new Building__c(Address_Postal_Code__c = '94043', Address_City__c = 'Mountain View', 
                                             Address_Street__c = '1600 Amphitheatre Parkway', Address_State__c = 'CA');
        insert building;
        
        Test.startTest();
            BuildingGeolocation.getLocation(building.Id);
        Test.stopTest();
        Building__c updatedBuilding = [SELECT Id, Location__latitude__s,  Location__longitude__s
                                    FROM Building__c WHERE Id =: building.Id];
        System.assertEquals(37.4224764, updatedBuilding.Location__latitude__s);
        System.assertEquals(-122.0842499, updatedBuilding.Location__longitude__s);
    }    

    @isTest static void testUpdateBuilding(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetBuildingGeolocationResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);        
        
        Building__c building = new Building__c (Address_Postal_Code__c = '94043', Address_City__c = 'Mountain View', 
                                                Address_Street__c = '1600 Amphitheatre Parkway');
        insert building;
        building.Address_State__c = 'CA';
        update building;
        
        Test.startTest();
            BuildingGeolocation.getLocation(building.Id);
        Test.stopTest();
        Building__c updatedBuilding = [SELECT Id, Location__latitude__s,  Location__longitude__s
                                    FROM Building__c WHERE Id =: building.Id];
        System.assertEquals(37.4224764, updatedBuilding.Location__latitude__s);
        System.assertEquals(-122.0842499, updatedBuilding.Location__longitude__s);
    }    
}
